# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Repo para la plantilla de documento de Consulta Teleco. Creada con la basde del repo de Rafa Bretón para el SPT

### How do I get set up? ###

* Básicamente habría que tocar el archivo principal: 
+ "0_documento_xxxx.tex", cambiando el nombre deseado y editando según preferencias. (Ojo: aquí se dice si queremos un tipo de documento largo (título e índice separados) o corto (título + índice en primera página)

+ \tex\0_definiciones.tex Con las definiciones utilizadas en el texto.


### Who do I talk to? ###

* Repo owner or admin: Jesús Maya Hurtado